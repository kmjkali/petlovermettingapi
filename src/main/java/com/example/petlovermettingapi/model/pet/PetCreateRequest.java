package com.example.petlovermettingapi.model.pet;


import com.example.petlovermettingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {

    private Member member;
    private String dogName;
    private Integer fee;

}
