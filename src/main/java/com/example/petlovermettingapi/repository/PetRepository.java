package com.example.petlovermettingapi.repository;

import com.example.petlovermettingapi.entity.Pet;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;




public interface PetRepository extends JpaRepository<Pet,Long> {
}
