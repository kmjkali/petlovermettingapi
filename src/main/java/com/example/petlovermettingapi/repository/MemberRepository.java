package com.example.petlovermettingapi.repository;

import com.example.petlovermettingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Long> {
}
