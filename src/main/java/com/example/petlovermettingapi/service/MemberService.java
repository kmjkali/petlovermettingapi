package com.example.petlovermettingapi.service;

import com.example.petlovermettingapi.entity.Member;
import com.example.petlovermettingapi.model.member.MemberCreateRequest;
import com.example.petlovermettingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class MemberService {
    private  final MemberRepository memberRepository;

    public Member getData(long id) {
        return  memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();

        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);


    }


}
