package com.example.petlovermettingapi.service;


import com.example.petlovermettingapi.entity.Member;
import com.example.petlovermettingapi.entity.Pet;
import com.example.petlovermettingapi.model.pet.PetCreateRequest;
import com.example.petlovermettingapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member, PetCreateRequest request){

        Pet addData = new Pet();

        addData.setDogName(request.getDogName());
        addData.setFee(request.getFee());
        addData.setMember(member);

        petRepository.save(addData);
    }

}
