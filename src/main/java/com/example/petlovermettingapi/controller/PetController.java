package com.example.petlovermettingapi.controller;


import com.example.petlovermettingapi.entity.Member;
import com.example.petlovermettingapi.model.pet.PetCreateRequest;
import com.example.petlovermettingapi.service.MemberService;
import com.example.petlovermettingapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {

    private final PetService petService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")

    public String setPet(@PathVariable long memberId, @RequestBody PetCreateRequest request){

        Member member = memberService.getData(memberId);
        petService.setPet(member,request);

        return "ok";
    }


}
