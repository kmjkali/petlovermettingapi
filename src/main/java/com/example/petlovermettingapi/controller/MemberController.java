package com.example.petlovermettingapi.controller;

import com.example.petlovermettingapi.model.member.MemberCreateRequest;
import com.example.petlovermettingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")

    private String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "ok";
    }

}
